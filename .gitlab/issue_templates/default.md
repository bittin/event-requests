<!---Thank you for your interest in applying for FOSDEM 2023 travel and accommodation funding from Fedora. Use this form to request pre-approval for funding requests for travel and accommodations. Please answer accurately as the information provided will be used in assessing whether the amenity provided complies with applicable laws related to anti-bribery and undue influence. Please expect 3-4 weeks of processing time.

NOTE: Make sure your issue is marked as confidential. This way, only members of the Fedora Mindshare Committee will receive your request. This box should be checked when you submit the issue: "This issue is confidential and should only be visible to team members with at least Reporter access."

If you have questions, send them by email to fcaic@fedoraproject.org.--->

This application form is for FOSDEM 2023 in Brussels, Belgium. The event happens on Saturday, 4 February and Sunday, 5 February.


## Place of work

* **Full name**:
* **Dates for requested amenities**:
* **Name of your employer**:
* **Is your employer a government entity, state-owned entity, or commercial entity (choose one)?**:
* **Country where you work**:
* **Your title at place of work**:


## Amenities

Please provide an estimate of each expense you are requesting in USD values as well as a short description for any relevant fields related to your request. Make sure to include any related taxes, gratuities, and fees.

* **Meals (including link to restaurant)**:
* **Entertainment(including link to venue)**:
* **Travel(including method of travel, origin and final destinations, any connecting flights, and dates of departure/return)**:
* **Accommodation**:


## About FOSDEM

<!---A condition of receiving Fedora funding is required participation with Fedora Community presences at FOSDEM. At FOSDEM 2023, Fedora has applied for a stand (i.e. community booth) and will co-run the Distributions Devroom. You can choose one or both options as part of your required participation. Additionally, if you will present a session and your session is related to Fedora, this can also count as part of your required participation.--->

* **Are you willing to spend between 30% and 60% of your time at FOSDEM as a Fedora volunteer?**: yes/no
* **Are you willing to help with the following areas:**
    * **Fedora stand**: yes/no
    * **Distributions devroom**: yes/no
    * **Speaking or presenting as a Fedora contributor**: yes/no
* _Optional_: **Are there other ways you could help Fedora at FOSDEM not mentioned above? If yes, list these activities**:

**Personal statement**:
<!---Use this to write a few sentences about why your funding application should be accepted. Why do you want to attend FOSDEM with Fedora? If you answered the previous optional question, you can describe those activities here.--->


## Additional information

* **Are you a Red Hat associate or contractor?**: yes/no
* **Have you been provided with Fedora/Red Hat funding in the past 12 months?**: yes/no
* **Does your place of work have any rules about the receipt of business amenities? If yes, please describe**.
* **Are you bringing a guest? If yes, will Fedora be paying for any of the guests expenses?**:
* **Are there any opportunities such as procurements, RFPs, or tenders between your place of work/organization and Fedora/Red Hat?**:
* **Is there any additional information that we should be aware of?**:


<!---DO NOT EDIT BELOW THIS LINE!--->

/confidential
