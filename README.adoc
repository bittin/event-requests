= FOSDEM 2023 Funding Applications
:license-image: https://licensebuttons.net/l/by-sa/4.0/88x31.png
:license-legal: https://creativecommons.org/licenses/by-sa/4.0/
:license-shield: https://img.shields.io/badge/License-CC%20BY-SA%204.0-blue.svg
:toc:

[link={license-legal}]
image::{license-shield}[License: Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)]

_Applications for Fedora contributors to apply for FOSDEM 2023 travel and accommodation funding_.


[[about]]
== About this repository

The purpose of this repository is to manage funding requests for Fedora contributors to attend FOSDEM 2023.
It is an experiment to develop an open, public process for funding applications that would be approved by the https://docs.fedoraproject.org/en-US/mindshare-committee/[Fedora Mindshare Committee].
FOSDEM 2023 will be hosted in Brussels, Belgium from Saturday, 4 February to Sunday, 5 February.

See the https://gitlab.com/fedora/mindshare/fosdem-2023/-/issues[GitLab issues] for a list of open applications.


[[apply]]
== How to apply

*A process will be described here soon*.
In short, an applicant will be required to submit a new issue using the default template provided.

Personal information (e.g. addresses, financial information, and any other personally-identifiable information) should *NOT* be stored here, or should be kept to an absolute minimum.


[[legal]]
== Legal

[link={license-legal}]
image::{license-image}[License: Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)]

All content in this repository is shared under the {license-legal}[Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0) license].
